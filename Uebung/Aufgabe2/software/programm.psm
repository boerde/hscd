
        CONSTANT IO_Port,  00
        CONSTANT LED0,     01
        CONSTANT LED1,     02
        CONSTANT BNT0,     04
        
        CONSTANT COM_Port, 01
        CONSTANT TXD,      01
        CONSTANT RXD,      01
	CONSTANT RAM_ADDRESS_1,		02	; A0 - A7
	;CONSTANT RAM_ADDRESS_2,		03	; A8 - A10
	CONSTANT RAM,			04	; RAM Block

; Register Mapping
; alle acht Register s0 bis s7 stehen der ISR zur Verfuegung
        NAMEREG s0, IOR
        NAMEREG s1, TMP
        NAMEREG s2, BYTE_READY ; Set if one byte is ready to be sent
        NAMEREG s3, BYTE_READ
	NAMEREG s4, CNT_CYCLES
	NAMEREG s5, CNT_BITS
	NAMEREG s6, BYTE
	NAMEREG s7, STATE_BIT
	NAMEREG s8, BIT_SEND	;Number of bit send
	NAMEREG s9, BIT_SEND_CNT;
	NAMEREG sa, BYTE_TO_SEND 
	NAMEREG sb, CNT_POSITIVE
	NAMEREG sc, CNTH
	NAMEREG sd, CNTL
	NAMEREG se, ONE
	NAMEREG sf, ZERO
; alle acht Register s8 bis sF stehen der Main-Funktion zur Verfuegung

; --------------------------------------------------------------------------------------------

        ADDRESS 000
; Hauptfunktion
start:  AND    CNTH, 00   ; Initialisierung der Zaehlers ...
        AND    CNTL, 00   ; in der ISR 
	LOAD STATE_BIT, 00; START_BIT not found yet
	LOAD CNT_CYCLES, 08	;Cycles to wait from first low
	LOAD BYTE_READY, 00	;nothing to send
	LOAD ONE, 01
	LOAD ZERO, 00
	LOAD BYTE_READ, 10	; After 16 bytes send ram
	LOAD CNT_POSITIVE, 00	; BYTES saved in ram
	OUTPUT ZERO, RAM_ADDRESS_1
	;OUTPUT ZERO, RAM_ADDRESS_2
        ENABLE INTERRUPT

loop:   ; do nothing
        JUMP loop

;------------------------------------------------------------------
; RECEIVING BITS

WAIT_LOW:
		INPUT IOR, COM_Port
		COMPARE IOR, 00
		JUMP NZ, FINISHED	;no start bit
		LOAD STATE_BIT, 01	; start bit found
		LOAD CNT_CYCLES, 08	; wait 8 cycles
		RETURNI ENABLE	

INVALID_STARTBIT:
		LOAD STATE_BIT, 00	;search next startbit
		RETURNI ENABLE

WAIT_FOR_STARTBIT: 			
		SUB CNT_CYCLES, 01 	;decrease counter
		JUMP NZ, FINISHED	;Wait longer waited 8 cycles?	
		INPUT IOR, COM_Port
		COMPARE IOR, 00		;bit = 0?
		JUMP NZ, INVALID_STARTBIT	;No valid Startbit	
		LOAD CNT_CYCLES, 10	;loop for 16 cycles
		LOAD CNT_BITS, 08	;1 byte = 8 bit
		LOAD STATE_BIT, 02	;Startbit was found
		RETURNI ENABLE
WAIT_FOR_BIT:
		SUB CNT_CYCLES, 01 	;decrease counter
		JUMP Z, SAVE_BIT
		RETURNI ENABLE
SAVE_BIT:
		LOAD CNT_CYCLES, 10	; next bit in 16 cycles
		INPUT IOR, COM_Port	;read input

		COMPARE IOR, 00		;save input
		JUMP Z, SAVE_LOW
		JUMP NZ, SAVE_HIGH

BIT_SAVED:	SUB CNT_BITS, 01	;1 bit read
		JUMP NZ, FINISHED	;wait next bit

BYTE_SAVED:	
		CALL TEST_BYTE		
		LOAD STATE_BIT, 03	;Wait for stop Bit
		SUB BYTE_READ, 01	; saved 1 byte
		JUMP NZ, FINISHED	;get next bit
		LOAD BYTE_READ, 10	; reset counter

		OUTPUT ZERO, RAM_ADDRESS_1; Reset Address to start

		JUMP SEND_STARTBIT
		
STOP_BIT:
		SUB CNT_CYCLES, 01
		JUMP NZ, FINISHED
		LOAD CNT_CYCLES, 10
		LOAD STATE_BIT, 00
		RETURNI ENABLE

SAVE_HIGH:
		SR1 BYTE
		JUMP BIT_SAVED
			
SAVE_LOW:
		SR0 BYTE
		JUMP BIT_SAVED

;---------------------------------------------------------------
;SENDING

SEND_STARTBIT:	LOAD STATE_BIT, 05
		CALL SEND_LOW			; Output start bit
		LOAD CNT_CYCLES, 10	; wait 16 cycles
		LOAD BIT_SEND, 09		;no bits send
		INPUT BYTE_TO_SEND, RAM
		RETURNI ENABLE

SEND_BIT:
		SUB CNT_CYCLES, 01	;wait 16 cycles
		JUMP NZ, FINISHED
		LOAD CNT_CYCLES, 10	; set for next bit
		SR1 BYTE_TO_SEND		; shift

		CALL C, SEND_HIGH		; send carry
		CALL NC, SEND_LOW

		SUB BIT_SEND, 01 	; 1 bit was send
		JUMP NZ, FINISHED	; continue if there is more to send
		LOAD CNT_CYCLES, 10	; wait 16 cycles
		LOAD STATE_BIT, 06	; send stopp bit
		RETURNI ENABLE

SEND_STOPBIT:
		SUB CNT_CYCLES, 01	; wait 16 cycles
		JUMP NZ, FINISHED
		LOAD CNT_CYCLES, 10
		CALL SEND_HIGH		; stop bit = high
		INPUT TMP, RAM_ADDRESS_1
		SUB CNT_POSITIVE, 01
		JUMP Z, ENTER_READING_STATE
		ADD TMP, 01

SEND_STOPBIT2:	OUTPUT TMP, RAM_ADDRESS_1
		LOAD STATE_BIT, 04	; send next start bit
		RETURNI ENABLE

UNDERFLOW:	;INPUT IOR, RAM_ADDRESS_2	; Underflow routine unnecessary
		;SUB IOR, 01 
		;JUMP C, ENTER_READING_STATE
		;LOAD TMP, ff
		;OUTPUT IOR, RAM_ADDRESS_2
		;JUMP SEND_STOPBIT2

ENTER_READING_STATE:
		OUTPUT ZERO, RAM_ADDRESS_1
		;OUTPUT ZERO, RAM_ADDRESS_2
		LOAD STATE_BIT, 00
		RETURNI ENABLE

SEND_HIGH:
		OUTPUT ONE, COM_Port
		RETURN

SEND_LOW:
		OUTPUT ZERO, COM_Port
		RETURN
		
;-----------------------------------------------------------
;Testing the Value
;Has to be in '1-9' or 'A-Z' or 'a-z'

TEST_BYTE:
		;... FAIL |29 - 40| FAIL |41 - 5a| FAIL |60 - 7a| FAIL ...
		COMPARE BYTE, 30 	; has to be greater than 29 (30 is '1')
		RETURN C		; FAILED
		COMPARE BYTE, 3a	; if lesser is in '1-9'
		JUMP C, TEST_POSITIVE  ; SUCCESS
		COMPARE BYTE, 7b	; has to be lesser than 7b (7a is 'Z')
		RETURN NC		; FAILED	
		COMPARE BYTE, 61	; if greater is in 'a-z'
		JUMP NC, TEST_POSITIVE	; SUCCESS
		COMPARE BYTE, 41	; has to be greater 40 AND lesser 5a
		RETURN C		; FAIL
		COMPARE BYTE, 5b	;
		RETURN NC		; FAIL
TEST_POSITIVE:
		;LOAD BYTE_READY, 01	;byte ready
		INPUT IOR, RAM_ADDRESS_1	; Get Address to write
		;CALL C, OVERFLOW_CARRY	; unnecessary overflow routine
		ADD CNT_POSITIVE, 01	; 1 byte was saved
		OUTPUT BYTE, RAM	; save byte
	
		ADD IOR, 01		; increase next adress
		OUTPUT IOR, RAM_ADDRESS_1

		RETURN			;SUCCESS

OVERFLOW_CARRY:
		;INPUT IOR, RAM_ADDRESS_2
		;ADD IOR, 01
		;OUTPUT IOR, RAM_ADDRESS_2
		;RETURN

;--------------------------------------
; DEBUGGING
		
DEBUG_OUTPUT:
		LOAD TMP, 00
		OUTPUT TMP, 65
		LOAD TMP, 01
		OUTPUT TMP, 01
		RETURN
		
DEBUG_STATEBIT:
		COMPARE STATE_BIT, 00
		CALL Z, DEBUG_OUTPUT
		COMPARE STATE_BIT, 01	;START_BIT set?
		CALL Z, DEBUG_OUTPUT
		CALL Z, DEBUG_OUTPUT
		COMPARE STATE_BIT, 02	;
		CALL Z, DEBUG_OUTPUT
		CALL Z, DEBUG_OUTPUT
		CALL Z, DEBUG_OUTPUT
; --------------------------------------------------------------------------------------------

; --------------------------------------------------------------------------------------------
 
; Subroutine, die den Zustand des Buttons BTN0 auf die Leuchtdiode LED0 durchschaltet
BNT0LED0:
        INPUT  IOR,  IO_Port ; IO-Port einlesen
        INPUT  TMP,  IO_Port ; IO-Port einlesen
        AND    TMP,  BNT0    ; Zustand von BNT0 ermitteln
        SR0    TMP           ; auf die Position ...
        SR0    TMP           ; ... von LED0 verschieben
        AND    IOR,  LED1    ; Zustand von LED1 ermitteln
        OR     IOR,  TMP     ; LED1 mit BNT0 konkatinieren
        OUTPUT IOR,  IO_Port ; den neuen Wert ueber IO-Port ausgeben 
        RETURN

; --------------------------------------------------------------------------------------------

; Subroutine, die die Leuchtdione LED1 mit ca. 1 Hz blinken laesst
BLINK:  ADD    CNTL, 01      ; den 16-Bit-Zaehler ... 
        ADDCY  CNTH, 00      ; .. inkrementieren
        JUMP   NC,   BLINK0  ; kein Ueberlauf -> exit, sonst
        INPUT  IOR,  IO_Port ; IO-Port einlesen
        XOR    IOR,  LED1    ; LED1 toggeln
        OUTPUT IOR,  IO_Port ; den neuen Wert ueber IO-Port ausgeben 
BLINK0: RETURN

; -------------------------------------------------------------------------------------------
        
        ADDRESS 300
; Interrupt-Service-Routine
ISR:    
	CALL    BNT0LED0
        CALL    BLINK
	COMPARE STATE_BIT, 02		;
	JUMP Z, WAIT_FOR_BIT
	COMPARE STATE_BIT, 05	;sent byte
	JUMP Z, SEND_BIT
	COMPARE STATE_BIT, 00
	JUMP Z, WAIT_LOW
	COMPARE STATE_BIT, 01		;START_BIT set?
       	JUMP Z, WAIT_FOR_STARTBIT 	
	COMPARE STATE_BIT, 03
	JUMP Z, STOP_BIT	
	COMPARE STATE_BIT, 04	;sent start bit
	JUMP Z, SEND_STARTBIT
	COMPARE STATE_BIT, 06	;sent stop bit
	JUMP Z, SEND_STOPBIT
	
FINISHED: RETURNI ENABLE

; --------------------------------------------------------------------------------------------

; Interrupt-Vektor
        ADDRESS 3FF
        JUMP ISR
