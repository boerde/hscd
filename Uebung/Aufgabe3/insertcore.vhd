LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

ENTITY insertcore IS
   GENERIC(RSTDEF: std_logic := '1');
   PORT(rst:  IN  std_logic;  -- reset, RSTDEF active
        clk:  IN  std_logic;  -- clock, rising edge active
        
        -- handshake signals
        strt: IN  std_logic;  -- start bit, high active
        done: OUT std_logic;  -- done bit, high active
        ptr:  IN  std_logic_vector(10 DOWNTO 0); -- pointer to vector
        len:  IN  std_logic_vector( 7 DOWNTO 0); -- length of vector
        
        WEB:  OUT std_logic; -- Port B Write Enable Output, high active
        ENB:  OUT std_logic; -- Port B RAM Enable, high active
        ADR:  OUT std_logic_vector(10 DOWNTO 0);  -- Port B 11-bit Address Output
        DIB:  IN  std_logic_vector( 7 DOWNTO 0);  -- Port B 8-bit Data Input
        DOB:  OUT std_logic_vector( 7 DOWNTO 0)); -- Port B 8-bit Data Output
END insertcore;

--PROCEDURE sort(a: INOUT string; n: positive) IS 
 --  VARIABLE key: character;
  -- VARIABLE j: integer; 
 --  VARIABLE i: natural range 1 to n; 
--   VARIABLE swp: boolean;
 --  VARIABLE flg: boolean;
 --  VARIABLE DIB: character; -- Ausgangsregister aus dem Speicher
   
  -- type TState is (S0, S1, S2, S3);
 --  variable state: TState := S0;
--BEGIN 
 --   loop
 --  case state is
 --  when S0 =>
   -- init
--      i := 1;
--      state := S1;
--   when S1 =>
   --ehemalige for schleife
 --     if i < n then
--        DIB:= a(i); 
--        j := i-1;
--        flg := false;
--        swp := false;
 --       state := S2;
 --     else
  --      return;
 --     end if;
      
 --     when S2 =>
 --     if not flg then
--         flg := true;
 --        key := DIB;
  --    end if;
 --     -- ehemalige while schleife
 --       if j >= 0 then
 --        DIB := a(j);
 --        state := S3;
  --      else
  --       IF swp THEN
 --           a(j+1) := key;
--		   end if;
--		   state:= S1;
--         i := i + 1;
--		  END IF;
		--  
	--	  when S3 =>
--		   IF DIB <= key THEN
           -- IF swp THEN
         --      a(j+1) := key;
		 --     end if;
     --       i := i + 1; 
   --         state := S1; 
 --        ELSE 
       --     swp := true;
     --       a(j+1) := DIB; 
   --         j := j - 1;
 --           state:=S2;
  --       end if;
 --        
 --      END CASE;
 --  END LOOP; 
--END PROCEDURE;

ARCHITECTURE verhalten OF insertcore IS
   TYPE TState IS (S0, S1, S2, S3);
   SIGNAL state, state0: TState;
   
   SIGNAL swp, swp0: std_logic;
   SIGNAL flg, flg0: std_logic;
   SIGNAL d, d0:     std_logic;
   SIGNAL i, i0, i1, i2: std_logic_vector(7 DOWNTO 0);
   SIGNAL j, j0, j1: std_logic_vector(8 DOWNTO 0);
   SIGNAL key, key0: std_logic_vector(7 DOWNTO 0);
   SIGNAL min, min0: std_logic_vector(7 DOWNTO 0);
   SIGNAL ofs:       std_logic_vector(7 DOWNTO 0);
   
   BEGIN
   
   done <= d;
   ADR  <= ptr + ofs;
   i1   <= i - 1;
   i2   <= i + 1;
   j1   <= j + 1;
   
   reg: PROCESS (rst, clk) IS
   BEGIN
      IF rst=RSTDEF THEN
         state <= S0;
         i     <= (OTHERS => '0');
         j     <= (OTHERS => '0');
         key   <= (OTHERS => '0');
         min   <= (OTHERS => '0');
         d     <= '0';
         flg   <= '0';
         swp   <= '0';
      ELSIF rising_edge(clk) THEN
         state <= state0;
         i     <= i0;
         j     <= j0;
         key   <= key0;
         min   <= min0;
         d     <= d0;
         flg   <= flg0;
         swp   <= swp0;
      END IF;
   END PROCESS;
   
   fsm: PROCESS (state, strt, len, i, i2,  i1, j, j1, d, key, min, flg, swp, dib) IS
   BEGIN
      state0 <= state;
      i0     <= i;
      j0     <= j;
      key0   <= key; 
      min0   <= min;
      d0     <= d; -- done bit
      flg0   <= flg; -- sperr flag
      swp0   <= swp; -- muss getauscht werden?
      
      ofs    <= i;   -- default (OTHERS => '0');
      WEB    <= '0';
      ENB    <= '0';
      DOB    <= (OTHERS => '0');
      
      CASE state IS
         when S0 =>
            if strt = '1' then
               d0    <= '0';
               i0    <=conv_std_logic_vector(1, i0'LENGTH);
               state0 <= S1;
            end if;
               
         when S1 =>
            if i < len then
               ENB   <= '1';
               flg0  <= '0';
               swp0  <= '0';
               j0    <= '0' & i1;
               state0<= S2;
            else
               d0 <= '1';
               state0<= S0;
            end if;
      
         when S2 =>
            if flg='0' then
               flg0  <='1';
               key0 <=DIB;
            end if;
            if j (j'LEFT) = '0' then      -- wenn j positiv
               ofs  	<= j(ofs'RANGE);
               ENB  <= '1';
               state0<= S3;
            else                                -- wenn j negativ
               IF swp='1' THEN
                  ofs   <= j1(ofs'RANGE);
                  DOB   <= key;
                  ENB   <= '1';
                  WEB  <= '1';
               end if;
               state0   <= S1;
               i0    <= i2;
            end if;
		  
         when S3 =>
            IF DIB <= key THEN 
               IF swp='1' THEN
                  ofs   <= j1(ofs'RANGE);
                  DOB   <= key;
                  ENB   <= '1';
                  WEB   <= '1';
               end if;
               i0    <= i2; 
               state0<= S1; 
            ELSE 
               swp0   <='1';
               ofs   <= j1(ofs'RANGE);
               DOB   <= DIB;
               ENB   <= '1';
               WEB   <= '1'; 
               j0    <= j - 1;
               state0<=S2;
            end if;
       END CASE;
   END PROCESS;

END verhalten;
