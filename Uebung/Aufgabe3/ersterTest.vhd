
ENTITY insertionsort IS
   -- empty
END insertionsort;

ARCHITECTURE verhalten OF insertionsort IS
  -- empty
BEGIN

   PROCESS
      CONSTANT N: natural := 20;
      TYPE string IS array(natural RANGE <>) OF character;
      CONSTANT g: string(0 TO N-1) := "1449BEKMMPQTZceffrvz";

      VARIABLE a: string(0 TO N-1) := "P91fQeZB4KvTMcrEfzM4";
      VARIABLE b: string(0 TO N-1) := "1449BEKMMPQTZceffrvz";
      VARIABLE c: string(0 TO N-1) := "zvrffecZTQPMMKEB9441";

PROCEDURE sort(a: INOUT string; n: positive) IS 
   VARIABLE key: character;
   VARIABLE j: integer; 
   VARIABLE i: natural range 1 to n; 
   VARIABLE swp: boolean;
   VARIABLE flg: boolean;
   VARIABLE DIB: character; -- Ausgangsregister aus dem Speicher
   
   type TState is (S0, S1, S2, S3);
   variable state: TState := S0;
BEGIN 
    loop
   case state is
   when S0 =>
   -- init
      i := 1;
      state := S1;
   when S1 =>
   --ehemalige for schleife
      if i < n then
        DIB:= a(i); 
        j := i-1;
        flg := false;
        swp := false;
        state := S2;
      else
        return;
      end if;
      
      when S2 =>
      if not flg then
         flg := true;
         key := DIB;
      end if;
      -- ehemalige while schleife
        if j >= 0 then
         DIB := a(j);
         state := S3;
        else
         IF swp THEN
            a(j+1) := key;
		   end if;
		   state:= S1;
         i := i + 1;
		  END IF;
		  
		  when S3 =>
		   IF DIB <= key THEN
            IF swp THEN
               a(j+1) := key;
		      end if;
            i := i + 1; 
            state := S1; 
         ELSE 
            swp := true;
            a(j+1) := DIB; 
            j := j - 1;
            state:=S2;
         end if;
         
       END CASE;
   END LOOP; 
END PROCEDURE;

BEGIN
      sort(a, n);
      ASSERT a=g   SEVERITY error;
      sort(b, n);
      ASSERT b=g   SEVERITY error;
      sort(c, n);
      ASSERT c=g   SEVERITY error;
      WAIT;
   END PROCESS;

END verhalten;